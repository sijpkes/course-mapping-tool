(function( $ ) {
	$.fn.expandTextarea = function() {
		
		$(this).keyup(function() {
	        this.style.overflow = 'hidden';
	        this.style.height = 0;
	        this.style.height = this.scrollHeight + 'px';
	    }, false);
		
	}
})( jQuery );