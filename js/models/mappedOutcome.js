var app = app || {};

app.mappedOutcome = Backbone.Model.extend({
	urlRoot: function() {
		var course_id = this.get("course_id");
		return "/course_mapping_tool/manager/index.php/summary/"+course_id;
	},
	defaults: {
		name: '',
	},
	initialize: function(param) {
		this.course_id = param.course_id;
	},
	
});
